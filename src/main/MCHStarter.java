package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import minConflictHeuristic.MinConflictHeuristic;
import objects.Activity;
import objects.Machine;
import utility.Loader;
import utility.Printer;

public class MCHStarter {

	private static List<Machine> machines;
	private static List<Activity> activitiesList;

	// PARAMETERS - alpha and p_noise
	static double[] alphaValues = { 0.5 };
	//static double[] pValues = { 0.1, 0.3, 0.5, 0.7 };
	static double[] pValues = { 0.1 };

	/**
	 * @param args
	 *            6 parameters:</br> args[0] = input file </br> args[1] = number
	 *            of machines </br> args[2] = power limit </br> args[3] = time
	 *            limit </br> args[4] = number of runs </br> args[5] = log file
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		if (args.length != 6) {
			System.err.println("Number of arguments should be 6!");
			System.err
					.println("inputFile numMachines power logFile timeLimit(min) numRuns");
			System.exit(-1);
		}

		activitiesList = Loader.loadActivities(args[0]);
		int numMachines = Integer.parseInt(args[1]);
		double power = Double.parseDouble(args[2]);
		int timeLimitMinutes = Integer.parseInt(args[3]);
		int numRuns = Integer.parseInt(args[4]);
		String logFile = args[5];

		Collections.sort(activitiesList); // for assigning activities to
											// machines
		// Printer.printActivities(activitiesList, false);

		MinConflictHeuristic mch;
		List<Activity> optimizedActivities;

		// this can be done once
		machines = initMachines(numMachines);
		assignActivitiesToMachines(); // ~1ms

		for (double alpha : alphaValues) {
			for (double rProb : pValues) {
				double fitness, fitnessMax = 0, fitnessMin = Double.MAX_VALUE, fitnessSum = 0;
				int iter = 0;
				long time, timeSum = 0;

				if (rProb == 0.0 || rProb >= 0.7)
					numRuns = 20;
				// ********* MCH **********
				while (iter++ < numRuns) {
					System.out.println("Run " + iter);
					Printer.printToFile("Run " + iter, logFile); // initialize
					mch = new MinConflictHeuristic(activitiesList, machines,
							power, timeLimitMinutes, logFile, alpha, rProb); // optimize
					time = System.currentTimeMillis();
					optimizedActivities = mch.start();
					timeSum += System.currentTimeMillis() - time;

					// statistics
					fitness = calculateFitness(optimizedActivities);
					if (fitness < fitnessMin)
						fitnessMin = fitness;
					if (fitness > fitnessMax)
						fitnessMax = fitness;
					fitnessSum += fitness;
					Printer.printToFile("\n ============================== \n",
							logFile);
				}

				Printer.printToFile(
						"alpha;rProb;runs;avgf;maxf;minf;avgopttime(ms)",
						logFile);
				Printer.printToFile(String.format("%f;%f;%d;%f;%f;%f;%f;",
						alpha, rProb, numRuns, fitnessSum / numRuns,
						fitnessMax, fitnessMin, (double) timeSum / numRuns),
						logFile);
			}
		}

	}

	/**
	 * Initialize the machines.
	 * 
	 * @param numMachines
	 *            number of machines
	 * @return list of initialized machines
	 */
	private static List<Machine> initMachines(int numMachines) {
		List<Machine> machines = new ArrayList<Machine>();
		for (int i = 0; i < numMachines; i++) {
			Machine m = new Machine(i);
			machines.add(m);
		}
		return machines;
	}

	/**
	 * Assign activities to machines (the greedy way). Activities are sorted by
	 * earliest time.
	 */
	private static void assignActivitiesToMachines() {
		for (Activity a : activitiesList) {
			for (Machine m : machines) {
				// because latest-1
				if (m.getEndBusyTime() <= a.getEarliest()) {
					a.setMachineID(m.getMachineID());
					m.addScheduledActivity(a);
					break;
				}
			}
		}
	}

	private static double calculateFitness(List<Activity> activities) {
		double profitSum = 0;
		for (Activity a : activities) {
			profitSum += a.getInstance().getProfit();
		}
		return profitSum;
	}

}
