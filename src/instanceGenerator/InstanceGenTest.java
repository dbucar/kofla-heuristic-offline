package instanceGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import objects.Activity;
import objects.ActivitySet;
import objects.Instance;
import objects.Machine;
import activityGenerator.ActivityGenerator;

public class InstanceGenTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ActivityGenerator agen = new ActivityGenerator(50, 8);
		List<Activity> activities = agen.generateActivities();

		List<Machine> machines = new ArrayList<Machine>();
		for (int i = 0; i < 32; i++) {
			Machine m = new Machine(i);
			machines.add(m);
		}

		int i = 0;
		for (Activity a : activities) {
			System.out.println(i++ + ": " + a.getEarliest() + " - " + a.getLatest() + ", duration: "
					+ (a.getLatest() - a.getEarliest()) + ", eDemand: " + a.getEnergyDemand());
		}

		System.out.println("machines size: " + machines.size());

		InstanceGenerator igen = new InstanceGenerator(activities, machines, 0.5);
		List<ActivitySet> activitySets = igen.generateInstances();

		try {
			// Create file
			FileWriter fstream = new FileWriter("output/out.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("actID | instID | mach | start | dur | speed | quota | power\n");
			for (ActivitySet as : activitySets) {
				for (Instance in : as.getInstances()) {
					out.write(in.getActivity().getActID() + " \t " + in.getInstanceID() + " \t " + in.getMachineID()
							+ " \t " + in.getStartTime() + " \t " + in.getDuration() + " \t " + in.getChargingSpeed()
							+ " \t " + in.getQuota() + " \t " + in.getPower() + " \n");
				}
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
