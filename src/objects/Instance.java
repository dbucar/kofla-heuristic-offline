package objects;

/**
 * Represents a charging time slot for a specific activity.
 * 
 * */
public class Instance implements Comparable<Instance> {

	private Activity activity; // TODO remove it
	private long instanceID;
	private int machineID; // TODO remove it
	private int startTime; // of charging
	private double chargingSpeed; // charging type (intensity)
	private int duration; // of charging
	private double cost; // 1/compPercentage == 1/quota
	private double quota; // percentage of demand satisfied {50, 75, 100}

	private double power; // required power during charging
	private double profit; // profit attained setting this instance active

	public Instance(Activity activity, long instID, int machineID, int startTime, double speed, int duration,
			double cost, double quota, double power, double profit) {
		this.activity = activity;
		this.instanceID = instID;
		this.machineID = machineID;
		this.startTime = startTime;
		this.chargingSpeed = speed;
		this.duration = duration;
		this.cost = cost;
		this.quota = quota;
		this.power = power;
		this.profit = profit;
	}

	public Instance(Instance instance) {
		this.activity = instance.getActivity();
		this.instanceID = instance.getInstanceID();
		this.machineID = instance.getMachineID();
		this.startTime = instance.getStartTime();
		this.chargingSpeed = instance.getChargingSpeed();
		this.duration = instance.getDuration();
		this.cost = instance.getCost();
		this.quota = instance.getQuota();
		this.power = instance.getPower();
		this.profit = instance.getProfit();
	}

	/**
	 * @return the activity
	 */
	public Activity getActivity() {
		return activity;
	}

	/**
	 * @param activity
	 *            the activity to set
	 */
	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	/**
	 * @return the instanceID
	 */
	public long getInstanceID() {
		return instanceID;
	}

	/**
	 * @param instanceID
	 *            the instanceID to set
	 */
	public void setInstanceID(long instanceID) {
		this.instanceID = instanceID;
	}

	/**
	 * @return the machine
	 */
	public int getMachineID() {
		return machineID;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachineID(int machineID) {
		this.machineID = machineID;
	}

	/**
	 * @return the startTime
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the chargingSpeed
	 */
	public double getChargingSpeed() {
		return chargingSpeed;
	}

	/**
	 * @param chargingSpeed
	 *            the chargingSpeed to set
	 */
	public void setChargingSpeed(double chargingSpeed) {
		this.chargingSpeed = chargingSpeed;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}

	/**
	 * @return the quota
	 */
	public double getQuota() {
		return quota;
	}

	/**
	 * @param quota
	 *            the quota to set
	 */
	public void setQuota(double quota) {
		this.quota = quota;
	}

	/**
	 * @return the power
	 */
	public double getPower() {
		return power;
	}

	/**
	 * @param power
	 *            the power to set
	 */
	public void setPower(double power) {
		this.power = power;
	}

	/**
	 * @return the profit
	 */
	public double getProfit() {
		return profit;
	}

	/**
	 * @param profit
	 *            the profit to set
	 */
	public void setProfit(double profit) {
		this.profit = profit;
	}

	@Override
	public int compareTo(Instance o) {
		if (!(o instanceof Instance)) {
			throw new ClassCastException("Invalid object");
		}

		final int LESS = -1;
		final int EQUAL = 0;
		final int MORE = 1;

		if (this.getProfit() < o.getProfit()) {
			return LESS;
		}

		if (this.getProfit() == o.getProfit()) {
			return EQUAL;
		}

		return MORE;
	}

}
