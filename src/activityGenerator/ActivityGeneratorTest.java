package activityGenerator;

import java.util.List;

import objects.Activity;


public class ActivityGeneratorTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ActivityGenerator agen = new ActivityGenerator(20, 8);
		List<Activity> activities = agen.generateActivities();
		int i = 0;
		for (Activity a : activities) {
			System.out.println(i++ + ": " + a.getEarliest() + " - " + a.getLatest() + ", duration: "
					+ (a.getLatest() - a.getEarliest() + 1) + ", eDemand: " + a.getEnergyDemand());
		}
	}

}
