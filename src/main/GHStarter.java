package main;

import greedyHeuristic.GreedyHeuristic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import objects.Activity;
import objects.Machine;
import utility.Loader;
import utility.Printer;

public class GHStarter {

	private static List<Machine> machines;
	private static List<Activity> activitiesList;

	// PARAMETER - alpha
	static double alpha = 0.5;

	/**
	 * @param args
	 *            4 parameters:</br> args[0] = input file </br> args[1] = number
	 *            of machines </br> args[2] = power limit </br> args[3] =
	 *            logfile
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		if (args.length != 4) {
			System.err.println("Number of arguments should be 4!");
			System.err.println("inputFile numMachines power logFile");
			System.exit(-1);
		}

		activitiesList = Loader.loadActivities(args[0]);
		int numMachines = Integer.parseInt(args[1]);
		double powerLimit = Double.parseDouble(args[2]);
		String logFile = args[3];

		Collections.sort(activitiesList); // for assigning activities to
											// machines
		// Printer.printActivities(activitiesList, false);

		// ********* GH **********
		machines = initMachines(numMachines);
		assignActivitiesToMachines();

		long time = System.currentTimeMillis();
		GreedyHeuristic gh = new GreedyHeuristic(activitiesList, machines, 8,
				powerLimit, alpha);
		List<Activity> scheduledActivities = gh.go();
		time = System.currentTimeMillis() - time;
		System.out.println("Optimization time: " + time + "ms");

		Printer.printToFile("\n ============================== \n", logFile);

		int[][] schedule = gh.getSchedule();
		Map<Integer, Activity> activityMap = gh.getActMap();

		Collections.sort(scheduledActivities);
		Printer.printActivitiesToFile(scheduledActivities, logFile);
		Printer.printScheduleToFile(schedule, logFile);
		Printer.printPowerConsumptionToFile(schedule, activityMap, powerLimit,
				logFile);
		Printer.printFitnessToFile(scheduledActivities, logFile);
		Printer.printToFile("Optimization time: " + time + "ms", logFile);
		Printer.printToFile("Unscheduled activities: "
				+ (activitiesList.size() - scheduledActivities.size()), logFile);

	}

	/**
	 * Initialize the machines.
	 * 
	 * @param numMachines
	 *            number of machines
	 * @return list of initialized machines
	 */
	private static List<Machine> initMachines(int numMachines) {
		List<Machine> machines = new ArrayList<Machine>();
		for (int i = 0; i < numMachines; i++) {
			Machine m = new Machine(i);
			machines.add(m);
		}
		return machines;
	}

	/**
	 * Assign activities to machines (the greedy way). Activities are sorted by
	 * earliest time.
	 */
	private static void assignActivitiesToMachines() {
		for (Activity a : activitiesList) {
			for (Machine m : machines) {
				// because latest-1
				if (m.getEndBusyTime() <= a.getEarliest()) {
					a.setMachineID(m.getMachineID());
					m.addScheduledActivity(a);
					break;
				}
			}
		}
	}

	private static double calculateFitness(List<Activity> activities) {
		double profitSum = 0;
		for (Activity a : activities) {
			profitSum += a.getInstance().getProfit();
		}
		return profitSum;
	}

}
