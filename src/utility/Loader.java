package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import objects.Activity;

public class Loader {

	public static List<Activity> loadActivities(String filename) {

		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(new File(filename)));
		} catch (FileNotFoundException e) {
			System.err.println("Specified file " + filename + " was not found!");
			System.exit(1);
		}

		List<Activity> activities = new ArrayList<Activity>();
		Activity act;

		while (true) {
			String line = null;

			try {
				line = reader.readLine();
			} catch (IOException e) {
				System.err.println("Error while reading instance file " + filename + "!");
				System.exit(1);
			}

			// if we read the empty line (P15.dat) or reach the end of file
			if (line.equals("") || line == null) {
				break;
			}

			// split on all whitespace chars
			String[] splitLine = line.trim().split("\\s+");

			// there are 10 columns
			if (splitLine.length != 10) {
				break;
			}

			// skip first line of input file
			if (splitLine[0].equals("act")) {
				continue;
			}

			// 0 = actID, 1 = earliest, 2 = latest, 5 = eDemand
			act = new Activity(Integer.parseInt(splitLine[0]), Integer.parseInt(splitLine[1]),
					Integer.parseInt(splitLine[2]), Integer.parseInt(splitLine[5]));

			activities.add(act);
		}

		try {
			reader.close();
		} catch (IOException e) {
			// TODO nothing
			e.printStackTrace();
		}

		return activities;
	}

}
