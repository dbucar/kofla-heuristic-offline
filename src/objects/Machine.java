package objects;

import java.util.ArrayList;
import java.util.List;

public class Machine {

	private int machineID;
	private int startBusyTime;
	private int endBusyTime;

	private List<Activity> scheduledActivities;

	public Machine(int id) {
		this.machineID = id;
		startBusyTime = 0;
		endBusyTime = 0;
		scheduledActivities = new ArrayList<Activity>();
	}

	/**
	 * @return the machineID
	 */
	public int getMachineID() {
		return machineID;
	}

	/**
	 * @param machineID
	 *            the machineID to set
	 */
	public void setMachineID(int machineID) {
		this.machineID = machineID;
	}

	/**
	 * @return the startBusyTime
	 */
	public int getStartBusyTime() {
		return startBusyTime;
	}

	/**
	 * @param startBusyTime
	 *            the startBusyTime to set
	 */
	public void setStartBusyTime(int startBusyTime) {
		this.startBusyTime = startBusyTime;
	}

	/**
	 * @return the endBusyTime
	 */
	public int getEndBusyTime() {
		return endBusyTime;
	}

	/**
	 * @param endBusyTime
	 *            the endBusyTime to set
	 */
	public void setEndBusyTime(int endBusyTime) {
		this.endBusyTime = endBusyTime;
	}

	/**
	 * @return the scheduledActivities
	 */
	public List<Activity> getScheduledActivities() {
		return scheduledActivities;
	}

	/**
	 * @param scheduledActivities
	 *            the scheduledActivities to set
	 */
	public void setScheduledActivities(List<Activity> scheduledActivities) {
		this.scheduledActivities = scheduledActivities;
	}

	/**
	 * @param a
	 *            the activity to add to the list of scheduledActivities
	 */
	public void addScheduledActivity(Activity a) {
		this.endBusyTime = a.getLatest();
		this.scheduledActivities.add(a);
	}
}
