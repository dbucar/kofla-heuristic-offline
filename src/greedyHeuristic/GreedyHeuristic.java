package greedyHeuristic;

import instanceGenerator.InstanceGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import objects.Activity;
import objects.Instance;
import objects.Machine;
import utility.Printer;

public class GreedyHeuristic {

	private List<Activity> activities;
	private List<Machine> machines;
	private double availablePower;
	private int[][] schedule;
	private int timeslots;
	private HashMap<Integer, Activity> activityMap;
	private double alpha;

	public GreedyHeuristic(List<Activity> activitiesList,
			List<Machine> machines, int workingHours, double power, double alpha) {
		this.activities = activitiesList;
		this.machines = machines;
		this.availablePower = power;
		this.timeslots = workingHours * 4;
		this.schedule = new int[machines.size()][timeslots];
		this.alpha = alpha;

		activityMap = new HashMap<Integer, Activity>();
		for (Activity a : activitiesList) {
			activityMap.put(a.getActID(), a);
		}
	}

	public List<Activity> go() {

		List<Activity> scheduledActivities = new ArrayList<Activity>();
		InstanceGenerator ig = new InstanceGenerator(activities, machines, alpha);
		List<Instance> greedyInstances;

		for (Activity a : activities) {
			greedyInstances = ig.getGreedyInstancesFor(a);
			if (greedyInstances.isEmpty()) {
				System.err.println("Parking too short for activity "
						+ a.getActID() + " [charging speed 3.7]");
				continue;
			}
			// sort by profit, i.e. completion
			Collections.sort(greedyInstances);
			Collections.reverse(greedyInstances);

			for (Instance i : greedyInstances) {
				a.setInstance(i);
				updateSchedule(a);

				boolean feasible = true;
				for (int t = 0; t < timeslots; t++) {
					double powerSum = 0;
					for (int row = 0; row < machines.size(); row++) {
						if (schedule[row][t] > 0) {
							powerSum += (activityMap.get(schedule[row][t]))
									.getInstance().getPower();
						}
					}
					if (powerSum > availablePower) {
						feasible = false;
						removeFromSchedule(a);
						break;
					}
				}
				if (feasible) {
					scheduledActivities.add(a);
					break;
				}
			}

		}

		Printer.printSchedule(schedule);
		Printer.printPowerConsumption(schedule, activityMap, availablePower);

		return scheduledActivities;

	}

	private void updateSchedule(Activity a) {
		int row = a.getMachineID();
		for (int t = a.getEarliest(); t < a.getLatest(); t++) {
			// if charging is active
			if (t >= a.getInstance().getStartTime()
					&& t < a.getInstance().getStartTime()
							+ a.getInstance().getDuration()) {
				schedule[row][t] = a.getActID();
			} else { // not charging, but machine m occupied by a
				schedule[row][t] = -a.getActID();
			}
		}

	}

	private void removeFromSchedule(Activity a) {
		int row = a.getMachineID();
		for (int t = a.getEarliest(); t < a.getLatest(); t++) {
			schedule[row][t] = 0;
		}
	}

	public int[][] getSchedule() {
		return schedule;
	}

	public Map<Integer, Activity> getActMap() {
		return activityMap;
	}
}
