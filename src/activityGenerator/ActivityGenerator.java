package activityGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import objects.Activity;


public class ActivityGenerator {

	/** intervals of 15 minutes */
	private int ticksPerHour = 4;
	// private int tickDuration = 15;
	private List<Integer> ticks;

	private int numberOfActivities;
	/** average activity time is 3 hours, i.e. 12 ticks */
	private int avgActivityDuration = 12;
	private int stddevActivityDuration = 2;

	private int workHours;

	private int minEDemand = 5;
	private int maxEDemand = 10;

	private Random rand = new Random(System.currentTimeMillis());

	/**
	 * @param numberOfActivities
	 * @param workHours
	 * */
	public ActivityGenerator(int numberOfActivities, int workHours) {

		this.numberOfActivities = numberOfActivities;
		this.workHours = workHours;

		ticks = new ArrayList<Integer>();

		for (int i = 0; i < workHours * ticksPerHour; i++) {
			ticks.add(i);
		}

		System.out.print("Ticks: ");
		for (Integer i : ticks) {
			System.out.print(i + " ");
		}
		System.out.println();

	}

	public List<Activity> generateActivities() {

		List<Activity> activities = new ArrayList<Activity>();
		int ticksSize = ticks.size();
		int maxEndTime = workHours * ticksPerHour - 1;

		for (int i = 0; i < numberOfActivities; i++) {
			int index = rand.nextInt(ticksSize);
			int startTime = ticks.get(index);
			int endTime = 0;
			// Gaussian with mean of avgActivityTime and stddev of
			// stddevActivityTime
			int activityDuration = (avgActivityDuration + (int) rand.nextGaussian() * stddevActivityDuration);
			// e.g. maxEndTime = 10, start = 7, actDur = 4 CAN FIT; 5 CAN NOT
			if (startTime + activityDuration - 1 > maxEndTime) {
				// keep the length, but shift the activity backwards
				endTime = maxEndTime;
				startTime = endTime - activityDuration + 1;
			} else {
				endTime = startTime + activityDuration - 1;
			}
			// generate random energy demand, ranging from minED to maxED
			int eDemand = minEDemand + rand.nextInt(maxEDemand - minEDemand + 1);

			Activity activity = new Activity(i, startTime, endTime, eDemand);
			activities.add(activity);
		}

		return activities;
	}
}
