package objects;

import java.util.List;

public class ActivitySet {

	private int actId;
	private List<Instance> instances;

	public ActivitySet(int actId, List<Instance> list) {
		this.actId = actId;
		this.instances = list;
	}

	/**
	 * @return the iD
	 */
	public int getActID() {
		return actId;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setActID(int actId) {
		this.actId = actId;
	}

	/**
	 * @return the instances
	 */
	public List<Instance> getInstances() {
		return instances;
	}

	/**
	 * @param instances
	 *            the instances to set
	 */
	public void setInstances(List<Instance> instances) {
		this.instances = instances;
	}

}
