package localRatioHeuristic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import objects.Activity;
import objects.ActivitySet;
import objects.Endpoint;
import objects.Instance;
import utility.Printer;

public class LocalRatioHeuristic {

	private final int START = 0;
	private final int END = 1;

	private double alpha = 2;

	private Map<Long, Instance> instanceMap;
	private Map<Long, Double> profits;
	private List<Endpoint> endpoints;
	private double availablePower;

	private int[][] schedule;

	// constructor - initialization
	public LocalRatioHeuristic(int machines, int workHours, double availablePower) {
		this.availablePower = availablePower;
		instanceMap = new HashMap<Long, Instance>();
		profits = new HashMap<Long, Double>();
		endpoints = new ArrayList<Endpoint>();
		this.schedule = new int[machines][workHours * 4];
	}

	// phase one
	public List<Instance> getFeasibleInstances(List<ActivitySet> activitySets) {

		// instancesToMap + initProfits + sortEndpoints
		initializeAll(activitySets);

		ActivitySet activitySet;
		Instance instance;
		long instanceID;
		double profitP;

		int numDeleted = 0;

		for (Endpoint e : endpoints) {
			instanceID = e.getInstanceID();
			instance = instanceMap.get(instanceID);
			if (e.getType() == END) {
				profitP = profits.get(instanceID);
				// original <= 0 - but only instances from start then
				if (profitP < 0) {
					numDeleted++;
					instanceMap.remove(instanceID);
				} else {
					// reduce profits for A(I)
					activitySet = getActivitySetFor(instance.getActivity().getActID(), activitySets);
					reduceProfitForActivity(activitySet.getInstances(), instanceID, profitP);
					// reduce profits for I(I)
					reduceProfitForOverlaps(instance, activitySets, profitP);
				}
			}
		}

		System.out.println("#Deleted: " + numDeleted);
		List<Instance> remainingInstances = new ArrayList<Instance>(instanceMap.values());
		System.out.println("#Remaining: " + remainingInstances.size());

		return remainingInstances;
	}

	// phase two
	public Map<Integer, Activity> constructSchedule(List<Instance> instances) {

		List<Instance> finalInstances = new ArrayList<Instance>();
		Instance instance;
		long instanceID;

		// initMachineOccupation();

		endpoints = new ArrayList<Endpoint>();
		sortEndpointsBackwards(instances);

		// total width of instances
		double W = 0;

		for (Endpoint e : endpoints) {

			instanceID = e.getInstanceID();
			instance = instanceMap.get(instanceID);

			if (instance != null) {
				if (e.getType() == END) {
					// not important, since machines already assigned
					// if (!isMachineOccupied(instance) && e.getType() == END) {
					if (W + instance.getPower() / availablePower <= 1.0) {
						finalInstances.add(instance);
						// setOccupation(instance.getMachineID(),
						// instance.getActivity().getEarliest(), instance
						// .getActivity().getLatest());
						// workaround for scaling to 0-1
						W += instance.getPower() / availablePower;
						deleteInstancesOfActivity(instance.getActivity().getActID(), instance.getInstanceID());
					} else {
						instanceMap.remove(instanceID);
					}
				} else {
					W -= instance.getPower() / availablePower;
				}
			}
		}

		Map<Integer, Activity> actsMap = new HashMap<Integer, Activity>();
		Activity a;
		for (Instance i : finalInstances) {
			a = i.getActivity();
			a.setInstance(i);
			actsMap.put(a.getActID(), a);
		}

		fillSchedule(finalInstances);

		Printer.printSchedule(schedule);
		Printer.printPowerConsumption(schedule, actsMap, availablePower);

		return actsMap;
	}

	// private void initMachineOccupation() {
	// for (int i = 0; i < machines; i++) {
	// for (int j = 0; j < timeslots; j++) {
	// machinesOccupation[i][j] = false;
	// }
	// }
	// }

	// private boolean isMachineOccupied(Instance instance) {
	// int startTime = instance.getActivity().getEarliest();
	// int endTime = instance.getActivity().getLatest();
	// int machineID = instance.getMachineID();
	//
	// for (int i = startTime; i <= endTime; i++) {
	// if (machinesOccupation[machineID][i] == true)
	// return true;
	// }
	// return false;
	// }

	// private void setOccupation(int machineID, int startTime, int endTime) {
	// for (int j = startTime; j <= endTime; j++) {
	// machinesOccupation[machineID][j] = true;
	// }
	// }

	private void fillSchedule(List<Instance> finalInstances) {
		Activity a;
		for (Instance i : finalInstances) {
			a = i.getActivity();
			int row = a.getMachineID();
			for (int t = a.getEarliest(); t < a.getLatest(); t++) {
				// if charging is active
				if (t >= i.getStartTime() && t < i.getStartTime() + i.getDuration()) {
					schedule[row][t - 1] = a.getActID();
				} else { // not charging, but machine m occupied by a
					schedule[row][t - 1] = -a.getActID();
				}
			}
		}

	}

	// instancesToMap + initProfits + sortEndpoints
	@SuppressWarnings("unchecked")
	private void initializeAll(List<ActivitySet> aSets) {
		Endpoint e;
		for (ActivitySet as : aSets) {
			for (Instance in : as.getInstances()) {
				// fill the instance map
				instanceMap.put(in.getInstanceID(), in);
				// fill the profits map
				profits.put(in.getInstanceID(), in.getProfit());
				// fill the endpoints list
				int startTime = in.getStartTime();
				int endTime = startTime + in.getDuration() - 1;

				e = new Endpoint(startTime, START, in.getInstanceID());
				endpoints.add(e);
				e = new Endpoint(endTime, END, in.getInstanceID());
				endpoints.add(e);
			}
		}
		Collections.sort(endpoints);

		System.out.println("#Instances: " + instanceMap.size());
		// System.out.println("#Profits: " + profits.size());
		// System.out.println("#Endpoints: " + endpoints.size());

	}

	@SuppressWarnings("unchecked")
	private void sortEndpointsBackwards(List<Instance> instances) {
		Endpoint e;
		for (Instance in : instances) {
			int startTime = in.getStartTime();
			// int endTime = startTime + in.getDuration() - 1; -- had to remove
			// -1 because of the infeasibility in border cases!
			int endTime = startTime + in.getDuration();

			e = new Endpoint(startTime, START, in.getInstanceID());
			endpoints.add(e);
			e = new Endpoint(endTime, END, in.getInstanceID());
			endpoints.add(e);
		}
		Collections.sort(endpoints);
		Collections.reverse(endpoints);
	}

	private ActivitySet getActivitySetFor(int activityID, List<ActivitySet> activitySets) {
		for (ActivitySet as : activitySets) {
			if (as.getActID() == activityID) {
				return as;
			}
		}
		return null;
	}

	private void reduceProfitForActivity(List<Instance> instances, long instanceID, double profitP) {
		double profit;
		for (Instance in : instances) {
			if (instanceID == in.getInstanceID()) {
				continue;
			}
			profit = profits.get(in.getInstanceID());
			profit -= profitP;
			profits.put(in.getInstanceID(), profit);
		}
	}

	private void reduceProfitForOverlaps(Instance instance, List<ActivitySet> activitySets, double profitP) {

		int actID = instance.getActivity().getActID();
		int start = instance.getActivity().getEarliest();
		int end = instance.getActivity().getLatest();
		int startPrime, endPrime;
		double profit;

		for (ActivitySet as : activitySets) {
			// belong to other activities
			if (as.getActID() != actID) {
				for (Instance in : as.getInstances()) {
					// on the same machine
					if (instance.getMachineID() == in.getMachineID()) {
						startPrime = in.getActivity().getEarliest();
						endPrime = in.getActivity().getLatest();

						// intersecting
						if (!(endPrime < start || end < startPrime)) {
							profit = profits.get(in.getInstanceID());
							// also walkaround for scaling power demand to 0-1
							profit -= alpha * (in.getPower() / availablePower) * profitP;
							profits.put(in.getInstanceID(), profit);
						}
					}
				}
			}
		}
	}

	private void deleteInstancesOfActivity(long actID, long instID) {

		List<Long> toDelete = new ArrayList<Long>();

		for (Long id : instanceMap.keySet()) {
			Instance in = instanceMap.get(id);
			if (in.getActivity().getActID() == actID && instID != in.getInstanceID()) {
				toDelete.add(id);
			}
		}
		// must do it this way, otherwise set iterator error (modify)
		for (long id : toDelete) {
			instanceMap.remove(id);
		}
	}
	
	public int[][] getSchedule (){
		return schedule;
	}

}
