package minConflictHeuristic;

import instanceGenerator.InstanceGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import objects.Activity;
import objects.Instance;
import objects.Machine;
import utility.Printer;

public class MinConflictHeuristic {

	private final int TIMESLOTS = 32;

	private Random rand;
	private InstanceGenerator ig;
	private List<Machine> machines;
	private List<Activity> activitiesList;
	private Map<Integer, Activity> activityMap = new HashMap<Integer, Activity>();
	private double powerLimit;
	private int timeLimitMinutes;
	private String logFile;
	private int[][] schedule;
	private double rProb;

	public MinConflictHeuristic(List<Activity> activities,
			List<Machine> machines, double powerLimit, int timeLimitMinutes,
			String logFile, double alpha, double rProb) {
		this.activitiesList = activities;
		this.machines = machines;
		this.powerLimit = powerLimit;
		this.timeLimitMinutes = timeLimitMinutes;
		this.logFile = logFile;
		this.rProb = rProb;

		this.rand = new Random(System.currentTimeMillis());
		this.ig = new InstanceGenerator(activitiesList, machines, alpha);

		assignInitialInstances();

		// map for quick fetching of charging activities
		for (Activity a : activitiesList) {
			activityMap.put(a.getActID(), a);
		}

		// solution representation
		schedule = new int[machines.size()][TIMESLOTS];
		fillSchedule();
	}

	@SuppressWarnings("unchecked")
	public List<Activity> start() {

		// printOut(false);
		// printToFile(false);

		long time = System.currentTimeMillis();
		// optimization
		optimize(powerLimit, timeLimitMinutes);
		// System.out.println("Optimization time: " +
		// (System.currentTimeMillis() - time) + "ms");
		time = System.currentTimeMillis() - time;
		System.out.println("Optimization time: " + time + "ms");

		activitiesList = new ArrayList<Activity>(activityMap.values());
		Collections.sort(activitiesList);

		// end = true
		// printOut(true);
		printToFile(true, time);

		return activitiesList;

	}

	private void printToFile(boolean end, long time) {
		Printer.printActivitiesToFile(activitiesList, logFile);
		Printer.printScheduleToFile(schedule, logFile);
		Printer.printPowerConsumptionToFile(schedule, activityMap, powerLimit,
				logFile);
		if (end) {
			Printer.printFitnessToFile(activitiesList, logFile);
			Printer.printToFile("Optimization time: " + time + "ms", logFile);
		}
	}

	private void printOut(boolean end) {
		// Printer.printActivities(activitiesList, true);
		// Printer.printSchedule(schedule);
		Printer.printPowerConsumption(schedule, activityMap, powerLimit);
		if (end) {
			Printer.printFitness(activitiesList);
		}
	}

	/**
	 * Fills the timetable.
	 */
	private void fillSchedule() {
		for (Machine m : machines) {
			for (Activity a : m.getScheduledActivities()) {
				updateSchedule(a);
			}
		}
	}

	/**
	 * Updates the schedule with the given activity.
	 * 
	 * @param a
	 *            activity to update
	 */
	private void updateSchedule(Activity a) {
		int row = a.getMachineID();
		for (int t = a.getEarliest(); t < a.getLatest(); t++) {
			// if charging is active
			if (t >= a.getInstance().getStartTime()
					&& t < a.getInstance().getStartTime()
							+ a.getInstance().getDuration()) {
				schedule[row][t] = a.getActID();
			} else { // not charging, but machine m occupied by a
				schedule[row][t] = -a.getActID();
			}
		}

	}

	/**
	 * Optimizes the initial schedule.</br> Looks for timeslots with power
	 * constraint violation, chooses an activity to shift, updates the schedule.
	 * </br> Runs until no more violations exist.
	 * 
	 * @param power
	 *            the power limit
	 * @return list of optimized activities
	 */
	private void optimize(double power, int timeLimitMinutes) {

		Map<Integer, List<Integer>> negativeSlots = getNegativeTimeslots(power);
		Set<Integer> timeKeys;
		List<Integer> chargingActivitiesIDs;
		Integer[] timeSlots;
		int selectedTimeslot, selectedActivityID;
		Activity activityToShift;
		Instance bestInstance;

		long counter = 0;
		long duration = timeLimitMinutes * 1000 * 60;
		long time = System.currentTimeMillis();

		while (negativeSlots.size() > 0
				&& System.currentTimeMillis() - time < duration) {

			// get a random timeslot with negative Power difference
			timeKeys = negativeSlots.keySet();
			timeSlots = timeKeys.toArray(new Integer[timeKeys.size()]);
			selectedTimeslot = timeSlots[rand.nextInt(timeSlots.length)];
			// randomly select one of the "active" activities
			chargingActivitiesIDs = negativeSlots.get(selectedTimeslot);
			selectedActivityID = chargingActivitiesIDs.get(rand
					.nextInt(chargingActivitiesIDs.size()));
			activityToShift = activityMap.get(selectedActivityID);
			// choose best neighbor
			bestInstance = bestImprovement(activityToShift, power);
			// bestInstance = firstImprovement(activityToShift, power);
			activityToShift.setInstance(bestInstance);
			// update activities map
			activityMap.put(selectedActivityID, activityToShift);

			// updateSchedule
			updateSchedule(activityToShift);

			counter++;
			negativeSlots = getNegativeTimeslots(power);
		}

		System.out.println("Number of Iterations: " + counter);
	}

	/**
	 * Get the best possible shift for the given activity.
	 * 
	 * @param activityToShift
	 * @param power
	 * @return best shift = best instance
	 */
	private Instance bestImprovement(Activity activityToShift, double power) {

		Instance best = activityToShift.getInstance();
		double[] initPowerDiff = getPowerDifferences(power);
		double[] bestPowerDiff = initPowerDiff;
		double[] newPowerDiff;
		double negSumBest = sumUpNegatives(bestPowerDiff);

		List<Instance> instances = ig.getInstancesFor(activityToShift);

		// in (rProb)% of cases take a random neighbor
		if (rand.nextDouble() < rProb) {
			return instances.get(rand.nextInt(instances.size()));
		}

		// SORT by profit - DESCENDING!
		Collections.sort(instances);
		Collections.reverse(instances);
		
		// RANDOM
		//Collections.shuffle(instances);

		// otherwise find the best improvement
		for (Instance newInstance : instances) {
			newPowerDiff = updatePowerDiff(initPowerDiff, activityToShift,
					newInstance);
			double negSumNew = sumUpNegatives(newPowerDiff);
			if (negSumNew > negSumBest
					|| (negSumNew == negSumBest && activityToShift
							.getInstance().getProfit() < newInstance
							.getProfit())) {
				best = newInstance;
				bestPowerDiff = newPowerDiff;
				negSumBest = negSumNew;
				if (negSumBest == 0) {
					break;
				}
			}
		}

		return best;
	}

	/**
	 * Get the first better shift for the given activity.
	 * 
	 * @param activityToShift
	 * @param power
	 * @return first improved instance
	 */
	private Instance firstImprovement(Activity activityToShift, double power) {

		Instance best = activityToShift.getInstance();
		double[] bestPowerDiff = getPowerDifferences(power);
		double[] newPowerDiff;
		double selectionPercentage = 0.1;

		List<Instance> instances = ig.getInstancesFor(activityToShift);

		// in (selectionPercentage)% of cases take a random neighbor
		if (rand.nextDouble() < selectionPercentage) {
			return instances.get(rand.nextInt(instances.size()));
		}

		// otherwise find the first improvement
		for (Instance newInstance : instances) {
			newPowerDiff = updatePowerDiff(bestPowerDiff, activityToShift,
					newInstance);
			if (sumUpNegatives(newPowerDiff) > sumUpNegatives(bestPowerDiff)) {
				return newInstance;
			}
		}
		return best;
	}

	/**
	 * Calculates the new power differences for the new instance.
	 * 
	 * @param oldPowerDiff
	 *            old power differences
	 * @param a
	 *            activity (with an assigned instance)
	 * @param i
	 *            new instance for activity a
	 * @return updated power differences
	 */
	private double[] updatePowerDiff(double[] oldPowerDiff, Activity a,
			Instance i) {

		double[] newPowerDiff = new double[oldPowerDiff.length];
		System.arraycopy(oldPowerDiff, 0, newPowerDiff, 0, oldPowerDiff.length);

		// deduce old Power usage == ADD it to the powerDifference
		for (int t = a.getInstance().getStartTime(); t < a.getInstance()
				.getStartTime() + a.getInstance().getDuration(); t++) {
			if (t >= 0) {
				newPowerDiff[t] = oldPowerDiff[t] + a.getInstance().getPower();
			}
		}
		// add new Power usage == DEDUCE it from the powerDifference 
		for (int t = i.getStartTime(); t < i.getStartTime() + i.getDuration(); t++) {
			newPowerDiff[t] = oldPowerDiff[t] - i.getPower();
		}

		return newPowerDiff;
	}

	private double sumUpNegatives(double[] powerDiff) {
		double sum = 0;
		for (int i = 0; i < powerDiff.length; i++) {
			if (powerDiff[i] < 0) {
				sum += powerDiff[i];
			}
		}
		return sum;
	}

	/**
	 * Slotwise calculation of (available power - used power).
	 * 
	 * @param availablePower
	 *            available power limit
	 * @return array of power differences (excess available)
	 */
	private double[] getPowerDifferences(double availablePower) {
		double[] pDiff = new double[TIMESLOTS];
		for (int t = 0; t < TIMESLOTS; t++) {
			double powerSum = 0;
			for (int row = 0; row < machines.size(); row++) {
				if (schedule[row][t] > 0) {
					powerSum += (activityMap.get(schedule[row][t]))
							.getInstance().getPower();
				}
			}
			pDiff[t] = availablePower - powerSum;
		}
		return pDiff;
	}

	/**
	 * Get timeslots where the power constraint is violated.
	 * 
	 * @param power
	 *            power limit
	 * @return list of timeslots
	 */
	private Map<Integer, List<Integer>> getNegativeTimeslots(double power) {

		Map<Integer, List<Integer>> negativeSlots = new HashMap<Integer, List<Integer>>();
		List<Integer> chargingActivities; // per timeslot

		for (int t = 0; t < TIMESLOTS; t++) {
			double powerSum = 0;
			chargingActivities = new ArrayList<Integer>();
			for (int row = 0; row < machines.size(); row++) {
				if (schedule[row][t] > 0) {
					powerSum += (activityMap.get(schedule[row][t]))
							.getInstance().getPower();
					chargingActivities.add(schedule[row][t]);
				}
			}
			// if we violated the power constraint == negative timeslot
			if (powerSum > power) {
				negativeSlots.put(t, chargingActivities);
			}
		}

		return negativeSlots;
	}

	/**
	 * Assignment of initial charging plans - instances - to activities.</br>
	 * Purely random selection among all possible instances is used. Sorting by
	 * profit tried, but worse fitness + increased runtime
	 */
	private void assignInitialInstances() {

		List<Instance> instances = new ArrayList<Instance>();

		for (Activity a : activitiesList) {

			// if not to reschedule
			// if (a.isNoReschedule()) {
			// if (a.getInstance() != null) {
			// continue;
			// }

			// otherwise, new reservation
			instances = ig.getInstancesFor(a);
			if (instances.isEmpty()) {
				System.out
						.println("Parking duration too short for reservation "
								+ a.getActID());
			}
			// assign a max-profit instance - hopefully better for incrementing
			// construction
			// Collections.sort(instances);
			// Collections.reverse(instances);
			// a.setInstance(instances.get(0));

			// OR a random one - better for construction from scratch!
			//a.setInstance(instances.get(rand.nextInt(instances.size())));

			// try with lowest speed - charge max for init
			// instances = ig.getGreedyInstancesFor(a);
			//
			// if (instances.size() > 0) {
			// Collections.sort(instances);
			// Collections.reverse(instances);
			// selectedInstance = instances.get(0);
			// }
			//
			// else {
			// instances = ig.getInstancesFor(a);
			// selectedInstance = instances.get(rand.nextInt(instances.size()));
			// }

			instances = ig.getInstancesFor(a);

			// get instance with MAX PROFIT
			Collections.sort(instances);
			Collections.reverse(instances);
			a.setInstance(instances.get(0));

			// or RANDOM
			//a.setInstance(instances.get(rand.nextInt(instances.size())));

		}
	}

}
