package localRatioHeuristic;

import instanceGenerator.InstanceGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import objects.Activity;
import objects.ActivitySet;
import objects.Instance;
import objects.Machine;

public class LRHMiniTest {

	private final static int MACHINES = 4;
	private final static int WORKINGHOURS = 8;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// generate activities
		List<Activity> activities = new ArrayList<Activity>();
		Activity activity;

		int startTime = 0;
		int endTime = 31;
		int eDemand = 20;
		activity = new Activity(0, startTime, endTime, eDemand);
		activities.add(activity);

		startTime = 0;
		endTime = 31;
		eDemand = 20;
		activity = new Activity(1, startTime, endTime, eDemand);
		activities.add(activity);

		startTime = 0;
		endTime = 31;
		eDemand = 20;
		activity = new Activity(2, startTime, endTime, eDemand);
		activities.add(activity);

		startTime = 0;
		endTime = 31;
		eDemand = 20;
		activity = new Activity(3, startTime, endTime, eDemand);
		activities.add(activity);

		// List<Activity> activities = readActivities("in.txt");

		int i = 0;
		for (Activity a : activities) {
			System.out.println(i++ + ": " + a.getEarliest() + " - "
					+ a.getLatest() + ", duration: "
					+ (a.getLatest() - a.getEarliest() + 1) + ", eDemand: "
					+ a.getEnergyDemand());
		}

		// initialize machines (i might not even need this)
		List<Machine> machines = new ArrayList<Machine>();
		for (i = 0; i < MACHINES; i++) {
			Machine m = new Machine(i);
			machines.add(m);
		}
		System.out.println("machines size: " + machines.size());

		// generate instances; alpha = 0.5
		InstanceGenerator igen = new InstanceGenerator(activities, machines, 0.5);
		List<ActivitySet> activitySets = igen.generateInstances();

		writeInstancesToFile(activitySets);

		// apply LRH
		LocalRatioHeuristic LRH = new LocalRatioHeuristic(MACHINES,
				WORKINGHOURS, 40);
		List<Instance> remainingInstances = LRH
				.getFeasibleInstances(activitySets);

		printInstances(remainingInstances, "Remaining Instances");

		// construct the schedule
//		List<Instance> finalInstances = LRH
//				.constructSchedule(remainingInstances);

//		printInstances(finalInstances, "Final Instances");
	}

	private static void printInstances(List<Instance> instances, String string) {
		System.out.println("\n" + string);
		System.out
				.println("actID | instID | mach | start | dur | speed | quota | power");
		for (Instance in : instances) {
			System.out.println(in.getActivity().getActID() + " \t "
					+ in.getInstanceID() + " \t " + in.getMachineID() + " \t "
					+ in.getStartTime() + " \t " + in.getDuration() + " \t "
					+ in.getChargingSpeed() + " \t " + in.getQuota() + " \t "
					+ in.getPower());
		}
	}

	private static void writeInstancesToFile(List<ActivitySet> activitySets) {
		try {
			// Write instances to file
			FileWriter fstream = new FileWriter("out.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("actID | instID | mach | start | dur | speed | quota | profit | power\n");
			for (ActivitySet as : activitySets) {
				for (Instance in : as.getInstances()) {
					out.write(in.getActivity().getActID() + " \t "
							+ in.getInstanceID() + " \t " + in.getMachineID()
							+ " \t " + in.getStartTime() + " \t "
							+ in.getDuration() + " \t " + in.getChargingSpeed()
							+ " \t " + in.getQuota() + " \t " + in.getProfit()
							+ " \t " + in.getPower() + " \n");
				}
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
