package instanceGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import objects.Activity;
import objects.ActivitySet;
import objects.Instance;
import objects.Machine;

public class InstanceGenerator {

	/** intervals of 15 minutes */
	private int ticksPerHour = 4;

	private double[] chargingSpeed = { 3.7, 8, 11 };
	private double[] completionPercentage = { 0.5, 0.75, 1.0 };

	private List<Activity> activities;
	private List<ActivitySet> activitySets;

	private Map<Integer, List<Instance>> instancesForAct;

	private double alpha;

	public InstanceGenerator(List<Activity> activities, List<Machine> machines,
			double alpha) {
		this.activities = activities;
		this.instancesForAct = new HashMap<Integer, List<Instance>>();
		this.alpha = alpha;
		// initInstances();
	}

	private void initInstances() {

		List<Instance> instances;
		Instance instance;
		double cost, quota, power, profit;
		int id = 0;

		for (Activity act : activities) {
			instances = new ArrayList<Instance>();
			for (double compPerc : completionPercentage) {
				for (double speed : chargingSpeed) {
					int duration = (int) Math.ceil(ticksPerHour * compPerc
							* act.getEnergyDemand() / speed);
					for (int start = act.getEarliest(); start <= act
							.getLatest() - duration; start++) {
						quota = compPerc;
						cost = 1 / compPerc;
						// profit or "weight", as in the literature
						profit = alpha * compPerc + (1 - alpha) / (0.5 * speed);
						// power calculation, instance "width", amount of
						// resource
						power = speed;

						instance = new Instance(act, id, act.getMachineID(),
								start, speed, duration, cost, quota, power,
								profit);
						instances.add(instance);

						id++;
					}
				}
			}
			instancesForAct.put(act.getActID(), instances);
		}

	}

	@Deprecated
	public List<Instance> getInstances(Activity act) {
		return instancesForAct.get(act.getActID());
	}

	public List<Instance> getInstancesFor(Activity act) {

		// speedup!
		if (instancesForAct.containsKey(act.getActID())) {
			return instancesForAct.get(act.getActID());
		}

		// System.out.println("pro�o!");
		List<Instance> instances = new ArrayList<Instance>();
		Instance instance;
		double cost, quota, power, profit;
		int id = 0;

		for (double compPerc : completionPercentage) {
			for (double speed : chargingSpeed) {
				int duration = (int) Math.ceil(ticksPerHour * compPerc
						* act.getEnergyDemand() / speed);
				for (int start = act.getEarliest(); start <= act.getLatest()
						- duration; start++) {
					quota = compPerc;
					cost = 1 / compPerc;
					// profit or "weight", as in the literature
					profit = alpha * compPerc + (1 - alpha) / (0.5 * speed);
					// power calculation, instance "width", amount of
					// resource
					power = speed;

					instance = new Instance(act, id, act.getMachineID(), start,
							speed, duration, cost, quota, power, profit);
					instances.add(instance);

					id++;
				}
			}
		}

		instancesForAct.put(act.getActID(), instances);
		return instances;
	}

	public List<Instance> getGreedyInstancesFor(Activity act) {

		List<Instance> instances = new ArrayList<Instance>();
		Instance instance;
		double cost, quota, power, profit;
		double speed = 3.7;
		int id = 0;

		for (double compPerc : completionPercentage) {
			int duration = (int) Math.ceil(ticksPerHour * compPerc
					* act.getEnergyDemand() / speed);
			if (duration <= act.getLatest() - act.getEarliest()) {
				// generate instances -- only from beginning of parking
				int start = act.getEarliest();
				quota = compPerc;
				cost = 1 / compPerc;
				profit = alpha * compPerc + (1 - alpha) / (0.5 * speed);
				power = speed; // amount of resource taken
				instance = new Instance(act, id, act.getMachineID(), start,
						speed, duration, cost, quota, power, profit);

				instances.add(instance);
				id++;

			}
		}

		return instances;
	}

	public List<ActivitySet> generateInstances() {

		Instance instance;
		double cost, quota, power, profit;
		activitySets = new ArrayList<ActivitySet>();

		int id = 0;

		for (Activity a : activities) {
			List<Instance> instances = new ArrayList<Instance>();
			// machine ALREADY ASSIGNED to activites in Starter!!
			// for (Machine m : machines) {
			// if (m.getEndBusyTime() <= a.getEarliest()) {
			for (double compPerc : completionPercentage) {
				for (double speed : chargingSpeed) {
					int duration = (int) Math.ceil(ticksPerHour * compPerc
							* a.getEnergyDemand() / speed);
					for (int start = a.getEarliest(); start <= a.getLatest()
							- duration; start++) {
						cost = 1 / compPerc;
						quota = compPerc;
						// profit or "weight", as in the literature
						profit = alpha * compPerc + (1 - alpha) / (0.5 * speed);
						// power calculation, the "width" of the
						// instance, amount of resource
						// power = getPowerPerTimeslot(start, duration, speed);
						power = speed;

						instance = new Instance(a, id, a.getMachineID(), start,
								speed, duration, cost, quota, power, profit);
						instances.add(instance);

						id++;
					}
				}
			}
			// }
			// }

			ActivitySet as = new ActivitySet(a.getActID(), instances);
			activitySets.add(as);
		}

		return activitySets;
	}

	private double getPowerPerTimeslot(int start, int duration, double speed) {

		// double minPower = 1000;
		// int max = start + duration;
		//
		// for (int i = start; i < max; i++) {
		// if (availablePower[i] < minPower) {
		// minPower = availablePower[i];
		// }
		// }

		// return speed/minPower;
		// speed [kWh] --> power needed in timeslot = speed/hours
		return speed;
		// return speed / AVAILPOWER;
	}
}
