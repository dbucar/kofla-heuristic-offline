package objects;

/**
 * Represents a time slot.
 * 
 * */
public class Activity implements Comparable {

	private int actID; // activity ID
	private int earliest; // arrival time of the vehicle as in the reservation
	private int latest; // leaving time of the vehicle as in the reservation
	// cannot be scheduled during latest! instance generation till latest-1
	
	private int energyDemand; // amount of kWh requested in the reservation
	private State state; // state, one of those defined in class State
	private int machineID; // assigned machine id
	private Instance instance; // assigned instance - charging plan

	/**
	 * @param startTime
	 * @param endTime
	 * */
	public Activity(int actID, int startTime, int endTime, int eDemand) {
		this.actID = actID;
		this.earliest = startTime;
		this.latest = endTime;
		this.energyDemand = eDemand;
	}

	/**
	 * @return the actID
	 */
	public int getActID() {
		return actID;
	}

	/**
	 * @param actID
	 *            the actID to set
	 */
	public void setActID(int actID) {
		this.actID = actID;
	}

	/**
	 * @return the earliest
	 */
	public int getEarliest() {
		return earliest;
	}

	/**
	 * @param earliest
	 *            the earliest to set
	 */
	public void setEarliest(int earliest) {
		this.earliest = earliest;
	}

	/**
	 * @return the latest
	 */
	public int getLatest() {
		return latest;
	}

	/**
	 * @param latest
	 *            the latest to set
	 */
	public void setLatest(int latest) {
		this.latest = latest;
	}

	/**
	 * @return the energyDemand
	 */
	public int getEnergyDemand() {
		return energyDemand;
	}

	/**
	 * @param energyDemand
	 *            the energyDemand to set
	 */
	public void setEnergyDemand(int energyDemand) {
		this.energyDemand = energyDemand;
	}

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the machineID
	 */
	public int getMachineID() {
		return machineID;
	}

	/**
	 * @param machine
	 *            the machineID to set
	 */
	public void setMachineID(int machineID) {
		this.machineID = machineID;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		// this.instance = new Instance(instance);
		this.instance = instance;
	}

	@Override
	public int compareTo(Object otherAct) {
		if (!(otherAct instanceof Activity)) {
			throw new ClassCastException("Invalid object");
		}

		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (this.getEarliest() < ((Activity) otherAct).getEarliest()) {
			return BEFORE;
		}

		if (this.getEarliest() == ((Activity) otherAct).getEarliest()) {
			return EQUAL;
		}

		return AFTER;
	}
}
