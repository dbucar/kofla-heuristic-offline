package objects;

public class Endpoint implements Comparable<Object> {

	private final int START = 0;
	private final int END = 1;

	private int timePoint;
	private int type;
	private long instanceID;

	public Endpoint(int time, int type, long l) {
		this.timePoint = time;
		this.type = type;
		this.instanceID = l;
	}

	/**
	 * @return the timePoint
	 */
	public int getTimePoint() {
		return timePoint;
	}

	/**
	 * @param timePoint
	 *            the timePoint to set
	 */
	public void setTimePoint(int timePoint) {
		this.timePoint = timePoint;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the instanceID
	 */
	public long getInstanceID() {
		return instanceID;
	}

	/**
	 * @param instanceID
	 *            the instanceID to set
	 */
	public void setInstanceID(int instanceID) {
		this.instanceID = instanceID;
	}

	@Override
	public int compareTo(Object otherEndpoint) {

		if (!(otherEndpoint instanceof Endpoint)) {
			throw new ClassCastException("Invalid object");
		}

		final int BEFORE = -1;
		final int AFTER = 1;

		int timePoint = ((Endpoint) otherEndpoint).getTimePoint();
		int type = ((Endpoint) otherEndpoint).getType();

		if (this.getTimePoint() < timePoint) {
			return BEFORE;
		}

		if (this.getTimePoint() == timePoint) {
			if (this.getType() == END && type == START) {
				return BEFORE;
			}
			else return 0;
		}

		return AFTER;
	}

}
