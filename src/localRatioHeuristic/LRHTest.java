package localRatioHeuristic;

import instanceGenerator.InstanceGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import objects.Activity;
import objects.ActivitySet;
import objects.Instance;
import objects.Machine;
import activityGenerator.ActivityGenerator;

public class LRHTest {

	private final static int MACHINES = 14;
	private final static int ACTIVITIES = 20;
	private final static int WORKINGHOURS = 8;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// generate activities
		ActivityGenerator agen = new ActivityGenerator(ACTIVITIES, WORKINGHOURS);
		List<Activity> activities = agen.generateActivities();

		// List<Activity> activities = readActivities("in.txt");

		int i = 0;
		for (Activity a : activities) {
			System.out.println(i++ + ": " + a.getEarliest() + " - "
					+ a.getLatest() + ", duration: "
					+ (a.getLatest() - a.getEarliest() + 1) + ", eDemand: "
					+ a.getEnergyDemand());
		}

		// initialize machines (i might not even need this)
		List<Machine> machines = new ArrayList<Machine>();
		for (i = 0; i < MACHINES; i++) {
			Machine m = new Machine(i);
			machines.add(m);
		}
		System.out.println("machines size: " + machines.size());

		// generate instances; alpha = 0.5
		InstanceGenerator igen = new InstanceGenerator(activities, machines, 0.5);
		List<ActivitySet> activitySets = igen.generateInstances();

		// writeInstancesToFile(activitySets);

		// apply LRH
		LocalRatioHeuristic LRH = new LocalRatioHeuristic(MACHINES,
				WORKINGHOURS, 40);
		List<Instance> remainingInstances = LRH
				.getFeasibleInstances(activitySets);

		printInstances(remainingInstances, "Remaining Instances");

		// construct the schedule
//		List<Instance> finalInstances = LRH
//				.constructSchedule(remainingInstances);

		// printInstances(finalInstances, "Final Instances");
	}

	// private static List<Activity> readActivities(String string) {
	// List<Activity> acts = new ArrayList<Activity>();
	// String line
	//
	// try {
	// BufferedReader reader = new BufferedReader(new FileReader(string));
	//
	// String line = reader.readLine();
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }

	//
	// return acts;
	// }

	private static void printInstances(List<Instance> instances, String string) {
		System.out.println("\n" + string);
		System.out
				.println("actID | earl | lat | inID | mach |start | dur | speed | quota | profit | power");
		for (Instance in : instances) {
			System.out.println(in.getActivity().getActID() + "\t"
					+ in.getActivity().getEarliest() + "\t"
					+ in.getActivity().getLatest() + "\t" + in.getInstanceID()
					+ "\t" + in.getMachineID() + "\t" + in.getStartTime()
					+ "\t" + in.getDuration() + "\t" + in.getChargingSpeed()
					+ "\t" + in.getQuota() + "\t" + in.getProfit() + "\t"
					+ in.getPower());
		}

	}

	private static void writeInstancesToFile(List<ActivitySet> activitySets) {
		try {
			// Write instances to file
			FileWriter fstream = new FileWriter("out.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("actID | earl | lat | inID | mach |start | dur | speed | quota | profit | power\n");
			for (ActivitySet as : activitySets) {
				for (Instance in : as.getInstances()) {
					out.write(in.getActivity().getActID() + " \t "
							+ in.getActivity().getEarliest() + " \t "
							+ in.getActivity().getLatest() + " \t "
							+ in.getInstanceID() + " \t " + in.getMachineID()
							+ " \t " + in.getStartTime() + " \t "
							+ in.getDuration() + " \t " + in.getChargingSpeed()
							+ " \t " + in.getQuota() + " \t " + in.getProfit()
							+ " \t " + in.getPower() + " \n");
				}
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}
}
