package utility;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import objects.Activity;
import objects.Instance;

public class Printer {

	public static void printActivities(List<Activity> activities,
			boolean printDetailed) {

		System.out.println("::: Activities :::");

		System.out
				.println("actID \tearl \tlat \tstart \tdur \teDem \tmach \tpwr \tquota");

		for (Activity a : activities) {

			if (printDetailed) {
				printActivity(a);
			}

			else {
				System.out.println(a.getActID() + "\t" + a.getEarliest() + "\t"
						+ a.getLatest() + "\t" + "\t" + "\t"
						+ a.getEnergyDemand());
			}
		}

		System.out.println();
	}

	public static void printActivitiesToFile(List<Activity> activities,
			String file) {

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw.println("::: Activities :::");

		pw.println("a \te \tl \ts \td \teD \tm \tp \tq");
		for (Activity a : activities) {
			pw.println(a.getActID() + "\t" + a.getEarliest() + "\t"
					+ a.getLatest() + "\t" + a.getInstance().getStartTime()
					+ "\t" + a.getInstance().getDuration() + "\t"
					+ a.getEnergyDemand() + "\t" + a.getMachineID() + "\t"
					+ a.getInstance().getPower() + "\t"
					+ a.getInstance().getQuota());
		}
		pw.println();

		pw.close();

	}

	public static void printActivity(Activity a) {
		System.out.println(a.getActID() + "\t" + a.getEarliest() + "\t"
				+ a.getLatest() + "\t" + a.getInstance().getStartTime() + "\t"
				+ a.getInstance().getDuration() + "\t" + a.getEnergyDemand()
				+ "\t" + a.getMachineID() + "\t" + a.getInstance().getPower()
				+ "\t" + a.getInstance().getQuota());
	}

	public static void printSchedule(int[][] schedule) {
		System.out.println("::: Schedule :::");

		for (int k = 0; k < schedule[0].length; k++) {
			System.out.printf("%3d ", k);
		}
		System.out.println("|  m");

		for (int k = 0; k < schedule[0].length; k++) {
			System.out.print("-----");
		}
		System.out.println();

		for (int i = 0; i < schedule.length; i++) {
			for (int j = 0; j < schedule[0].length; j++) {
				System.out.printf("%3d ", schedule[i][j]);
			}
			System.out.printf("|%3d\n", i); // machineID
		}

		System.out.println();
	}

	public static void printScheduleToFile(int[][] schedule, String file) {

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw.println("::: Schedule :::");

		for (int k = 0; k < schedule[0].length; k++) {
			pw.printf("%3d ", k);
		}
		pw.println("|  m");

		for (int k = 0; k < schedule[0].length; k++) {
			pw.print("-----");
		}
		pw.println("");

		for (int i = 0; i < schedule.length; i++) {
			for (int j = 0; j < schedule[0].length; j++) {
				pw.printf("%3d ", schedule[i][j]);
			}
			pw.printf("|%3d\n", i); // machineID
		}

		pw.println("");

		pw.close();

	}

	public static void printInstances(List<Instance> instances) {
		System.out.println("::: Activities :::");

		System.out
				.println("actID \tearl \tlat \tstart \tdur \teDem \tmach \tpwr \tquota");

		for (Instance i : instances) {
			printInstance(i);
		}

		System.out.println();
	}

	public static void printInstance(Instance i) {
		System.out.println(i.getActivity().getActID() + "\t"
				+ i.getActivity().getEarliest() + "\t"
				+ i.getActivity().getLatest() + "\t" + i.getStartTime() + "\t"
				+ i.getDuration() + "\t" + i.getActivity().getEnergyDemand()
				+ "\t" + i.getActivity().getMachineID() + "\t" + i.getPower()
				+ "\t" + i.getQuota());

	}

	public static void printPowerConsumption(int[][] schedule,
			Map<Integer, Activity> activityMap, double powerLimit) {
		double[] powerCons = new double[schedule[0].length];
		double soloPower;
		boolean violation = false;
		for (int col = 0; col < schedule[0].length; col++) {
			for (int row = 0; row < schedule.length; row++) {
				if (schedule[row][col] > 0) {
					soloPower = (activityMap.get(schedule[row][col]))
							.getInstance().getPower();
					powerCons[col] += soloPower;
				}
			}
			if (powerCons[col] > powerLimit) {
				violation = true;
			}
			System.out.printf("%3d ", Math.round(powerCons[col]));
		}
		System.out.println("| P");
		System.out.println("Power Limit: " + powerLimit);
		if (violation) {
			System.out.println("Power violation!");
		}
	}

	public static void printPowerConsumptionToFile(int[][] schedule,
			Map<Integer, Activity> activityMap, double powerLimit, String file) {

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double[] powerCons = new double[schedule[0].length];
		double soloPower;
		boolean violation = false;
		for (int col = 0; col < schedule[0].length; col++) {
			for (int row = 0; row < schedule.length; row++) {
				if (schedule[row][col] > 0) {
					soloPower = (activityMap.get(schedule[row][col]))
							.getInstance().getPower();
					powerCons[col] += soloPower;
				}
			}
			if (powerCons[col] > powerLimit) {
				violation = true;
			}
			pw.printf("%3d ", Math.round(powerCons[col]));
		}
		pw.println("| P");
		pw.println("Power Limit: " + powerLimit);
		if (violation) {
			pw.println("Power violation!");
		}

		pw.close();

	}

	public static void printFitness(List<Activity> activities) {
		double profitSum = 0;
		for (Activity a : activities) {
			profitSum += a.getInstance().getProfit();
		}

		System.out.println("Total Profit: " + profitSum);
	}

	public static void printFitnessToFile(List<Activity> activities, String file) {

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double profitSum = 0;
		for (Activity a : activities) {
			profitSum += a.getInstance().getProfit();
		}

		pw.println("Total Profit: " + profitSum);

		pw.close();
	}

	public static void printToFile(String string, String file) {
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(file, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw.println(string);

		pw.close();

	}

}
