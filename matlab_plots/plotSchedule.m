function plotSchedule(scheduleFile)
slotTime = 1; %minutes
timeFrame = 32; %minutes
slots = ceil(timeFrame/slotTime);
tick = 4;

scheduleFile = 'greedy_20_matlab.dat';
%Import data from file
DELIMITER = ' ';
HEADERLINES = 1;
% Import the file
newData1 = importdata(scheduleFile, DELIMITER, HEADERLINES);
% Create new variables in the base workspace from those fields.
vars = fieldnames(newData1);
for i = 1:length(vars)
    assignin('base', vars{i}, newData1.(vars{i}));
end
newScheduleData = newData1.data(1:8,1:end);
existingScheduleData = newData1.data(9:end,1:3);
physicalResources = size(existingScheduleData,1);


% Create figure
figure1 = figure;
% Create axes

xTimeTicks = 0:slotTime:timeFrame;
xTickLabelCellArray = cell(1,length(xTimeTicks));
for a=1:length(xTimeTicks)
    if(mod(a,3)==1)
        xTickLabelCellArray{a} = num2str(xTimeTicks(a));
    else
        xTickLabelCellArray{a} = '';
    end
end

axes1 = axes('Parent',figure1,...
    'YTick',0:physicalResources,...
    'XTick',xTimeTicks, 'XtickLabel', xTickLabelCellArray);
% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[0 timeFrame]);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0 physicalResources+1]);
grid

%Row indexes
IDIdxRow = 1;
resourceIdxRow = 7;
StartTimeWindowIdxRow = 2;
EndTimeWindowresourceIdxRow = 3;
chargeStartIdxRow = 4;
edemandIdxRow = 5;
chargeIntensity = 6;
completionIdxRow = 8;

%Plotting periods. Should be read from a CSV file (provided as input to this function)
for idx=1:size(newScheduleData,2)
    plotServerResource(newScheduleData(resourceIdxRow,idx), newScheduleData(StartTimeWindowIdxRow,idx)*slotTime, newScheduleData(EndTimeWindowresourceIdxRow,idx)*slotTime, 4*(newScheduleData(completionIdxRow,idx))-1);
    plotActivePeriod(newScheduleData(resourceIdxRow,idx), newScheduleData(chargeStartIdxRow,idx)*slotTime,   newScheduleData(chargeStartIdxRow,idx)*slotTime + newScheduleData(completionIdxRow,idx)*tick * newScheduleData(edemandIdxRow,idx)/newScheduleData(chargeIntensity,idx), ceil(newScheduleData(chargeIntensity,idx))/4);
    text(newScheduleData(StartTimeWindowIdxRow,idx)+0.5,newScheduleData(resourceIdxRow,idx),...
	[num2str(newScheduleData(IDIdxRow, idx))],...
	'HorizontalAlignment','center',... 
    'FontSize',9,...
    'Color',[1 1 1]);
end
NOW = 5;

for idx=1:size(existingScheduleData,1)
     if existingScheduleData(idx,3)>0 
         plotServerResource(existingScheduleData(idx, 1), NOW*slotTime, existingScheduleData(idx,3)*slotTime, 3);
         %plotActivePeriod(existingScheduleData(idx, 1), NOW*slotTime, existingScheduleData(idx,3)*slotTime, 1);
     end
end
function plotServerResource(server, startTime, endTime, colorId)
    colorCellArray={[0.756862759590149 0.866666674613953 0.776470601558685], [0,0,1],[0,1,1]};
    
    rectangle('Position',[startTime,server-0.5,endTime-startTime,1],'FaceColor',colorCellArray{colorId})

end

function plotActivePeriod(server, startTime, endTime, colorId)
    colorCellArray={[0,1,0], [1,1,0], [0,1,1],[1,0,0], [1,0,1]};
    rectangle('Position',[startTime,server-0.25,endTime-startTime,0.5],'FaceColor',colorCellArray{colorId});
end

end